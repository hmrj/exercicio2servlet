/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if6ae.exemplo.servlet;

import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author henrique
 */
@WebServlet(name = "LoginMan", urlPatterns = "/login")
public class LoginManager extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("login") == null ? req.getParameter("senha") == null : req.getParameter("login").equals(req.getParameter("senha"))){
//            resp.setHeader("Location", "/sucesso?login="+URLEncoder.encode(req.getParameter("login"), "UTF-8")+"&perfil="+URLEncoder.encode(req.getParameter("perfil"), "UTF-8"));
            String tipo = "";
            switch(Integer.parseInt(req.getParameter("perfil"))){
                case 1:
                    tipo = "Cliente";
                    break;
                case 2:
                    tipo = "Gerente";
                    break;
                case 3:
                    tipo = "Administrador";
                    break;
                default:
                    tipo = req.getParameter("perfil");
                    break;
            }
            resp.sendRedirect(req.getContextPath()+"/sucesso?login="+URLEncoder.encode(req.getParameter("login"), "UTF-8")+"&perfil="+URLEncoder.encode(, "UTF-8"));
        }else{
//            resp.setHeader("Location", "/erro.xhtml");
            resp.sendRedirect(req.getContextPath()+"/erro.xhtml");
        }
    }
    
}
